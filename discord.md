Ce document présente l'outil de communication Discord. [Rendez-vous
ici](https://discordapp.com) pour le télécharger et créer un compte.

### Table des matières

[Brève description](#brève-description)

[Utiliser un serveur](#utiliser-un-serveur)

[Créer son serveur et le configurer](#créer-son-serveur-et-le-configurer)

# Brève description

Discord est un logiciel gratuit de VoIP conçu initialement pour les
communautés de joueurs. Il fonctionne sur les systèmes d’exploitations
Windows, macOS, Linux, Android, iOS ainsi que sur les navigateurs
web. Ce logiciel est constitué de serveurs, hébergés par Discord, créés
par n'importe quel utilisateur. Vous retrouverez dans ces serveurs des
salons vocaux et textuels pour communiquer. Il est également possible de
communiquer en messages privés avec un utilisateur ou un groupe
d'utilisateur. Il suffit pour cela de cliquer sur le logo de discord au
dessus de la liste des serveurs.

# Utiliser un serveur
Cette partie décrit les différents outils de communication de Discord.

## Les salons textuels

![Text](https://i.imgur.com/236rrz8.png)

Ce sont les salons les plus utilisés, les personnes ne peuvent
communiquer ici que par écrit. Chaque salon a sa propre thématique (définie par
les administrateurs du serveur), à respecter pour une meilleure organisation.

### Les actions sur les messages

En faisant clic droit sur un message vous accédez aux options suivante
(ceci peut varier suivant les droits qui vous sont attribués sur le
serveur, [cf ici](#creer-son-serveur-et-le-configurer)) :
* **Ajouter une réaction** : permet d'ajouter un emoji sous le message

* **Epingler le message** : permet d'ajouter le message dans la liste de messages épinglés. Utile pour retrouver des messages importants

	![Message épinglé](https://i.imgur.com/bmoy8GF.png)

* **Citation** : raccourci permettant de citer un message

* **Marquer comme non lu** : permet de marquer un message comme non lu (utile si on veut se rappeler de lire un message)

* **Copier le lien du message** : peu utilisé, permet d'envoyer le lien
  d'un message à un utilisateur

* **Supprimer le message**

### Les "tags" ou mentions

Il est possible de mentionner une personne ou un groupe de personnes
([cf ici](#creer-son-serveur-et-le-configurer)) en précédant le pseudo
ou le nom du groupe d'un `@`. Cela aura pour effet d'envoyer une
notification aux personnes concernées et leur surlignera le message dans
le salon.  ***Appuyez sur `entrée` ou `tabulation` lorsque vous tapez `@nom` pour
sélectionner une personne ou un groupe***

![Liste des tags](https://i.imgur.com/Xj7dexx.png)

Il y a 2 mentions spéciales :

* `@everyone` : permet de mentionner **tout le monde** sur le serveur (à utiliser avec parcimonie)

* `@here` : permet de mentionner **toutes les personnes actuellement connectées** sur le salon

### Envoyer un fichier

Il est possible d'envoyer des fichiers (maximum 8MB) en appuyant sur le `+` à gauche de la barre de messages.

![Bouton +](https://i.imgur.com/FZEjvJU.png)

### Mise en page des messages

Pour plus de lisibilité, les messages textuels supportent la syntaxe
[MarkDown](https://support.discordapp.com/hc/fr/articles/210298617-Bases-de-la-mise-en-forme-de-texte-Markdown-mise-en-forme-du-chat-gras-italique-souligné).

## Les salons vocaux

![Vocal](https://i.imgur.com/qZPG3Xn.png)

Les salons vocaux vous permettent de parler avec d'autres membres du
serveur. Pour vous connecter, cliquez sur un salon vocal et pour vous
déconnecter, cliquez sur le bouton raccrocher.  Vous pouvez couper votre
micro en cliquant sur le micro et couper le son des autres en cliquant
sur le casque.

![Raccrocher](https://i.imgur.com/sIYJ94b.png)

### Partager son écran 

Discord offre une fonctionnalité permettant de partager son écran, soit
en entier, soit une seule fenêtre d'application. Lorsque vous êtes dans
un salon vocal, cliquez sur le bouton `Go Live` et sélectionnez ce que
vous souhaitez partager.

![Go live](https://i.imgur.com/TRUPE1y.png)

Un signal `En direct` est alors visible par les autres utilisateurs, à
côté de vous (dans le salon où vous êtes situé). En temps normal, vous
pouvez accueillir jusqu'à 10 spectateurs en direct. ***En raison des
conditions liées au COVID-19, Discord a fait passer cette limite de 10 à
50 pour la période de confinement.***

Pour participer à un direct, cliquez simplement sur la personne qui le diffuse, puis cliquez sur `Rejoindre`.

## Les catégories

Les salons peuvent être regroupés en *catégories*. Ceci permet de
regrouper les salons par thèmes (par exemples `important` ou
`cours`). Vous pouvez replier une catégorie pour y voir plus clair en
cliquant sur la flèche à côté du nom de la catégorie.

## Quelques astuces...

* Pensez à régler les paramètres de notification en cliquant sur un
  serveur ou un salon, par exemple pour éviter d'être harcelé par les notifications...

* Vous pouvez changer votre pseudo (si vous en avez les droits) en cliquant droit sur votre pseudo et `Changer le pseudo`

* Vous n'entendez pas bien une personne dans un salon vocal ? Cliquez droit sur elle et réglez son volume avec le curseur.

*Pour plus d'informations, n'hésitez pas à consulter* [le support de Discord](https://support.discordapp.com/hc/fr) *qui est très fourni.*

# Créer son serveur et le configurer
Pour créer son serveur, il suffit d'appuyer sur le gros `+` en dessous de la liste des serveurs.

![Créer son serveur](https://i.imgur.com/i42khOx.png)

Vous choisissez ensuite un nom et une icône (par défaut, les initiales
de votre serveur). Par défaut 2 salons et 2 catégories sont créés.

En premier lieu, vérifiez  les paramètres en cliquant sur le nom du serveur.

![Paramètres](https://i.imgur.com/aHDJrfR.png)

## Les paramètres

Contenu relativement intuitif. *A noter que `intégrations` `webhooks` et `widget` sont des options réservées aux développeurs.*

## Les rôles

Dans l'onglet rôles il vous est possible d'assigner des rôles aux
utilisateurs afin de gérer leurs droits sur le serveur. Il est aussi
possible de donner accès à des salons, uniquement à certains
utilisateurs par exemple (cf plus bas).

Attention : le rôle `@everyone` concerne **tous les utilisateurs du serveur**

Pour créer un rôle, appuyez sur le `+` à côté de `Rôles`. Vous pouvez
lui affecter une couleur repérer plus facilement les membres de ce
rôle. A noter que `Afficher les membres ayant ce rôles séparément [...]`
permet de mettre en avant les personnes ayant ce rôle dans la liste des
personnes connectées (à droite de la zone de messages). Je vous
conseille d'activer `Permettre à tout le monde de @mentionner ce rôle`
pour que le rôle soit mentionnable (cf les mentions).

## Les invitations

Pour ajouter des utilisateurs au serveur, cliquez sur `Créer une
invitation` à gauche d'un salon. Toutes les personnes qui cliqueront sur
le lien arriveront sur ce salon par défaut, donc choisissez un salon
approprié.

![Invitation](https://i.imgur.com/8fu6d1W.png)

Je vous conseille de créer une seule invitation et de la rendre inexpirable.

## Créer des salons / catégories

Pour créer des salons ou catégories, cliquez sur le nom du serveur et
sélectionnez `Créer un salon/catégorie`. Vous pouvez ordonner les salons
comme bon vous semble en glissant déposant. A noter que les salons
vocaux seront toujours situés en dessous des serveurs textuels.

Cliquez sur l'engrenage à côté du bouton d'invitation pour rentrer dans
les paramètres d'un salon. Vous pouvez donner des droits spécifiques à
des rôles pour un salon (valables uniquement pour ce salon) ou à une catégorie
(valables pour tous les salons de la catégorie). Par exemple, si vous
voulez faire un salon `annonce` où seuls les `@admins` peuvent parler,
mais qu'il soit visible par tous, configurez de la sorte :

![Config everyone](https://i.imgur.com/y2jmrVT.png)

pour `@everyone`

![Config admin](https://i.imgur.com/raOKVuF.png)

pour `@admins`

Vous pouvez aussi faire des salons accessibles uniquement par un certain groupe. Discord vous en offre la possibilité quand vous créez un salon avec l'option `Salon privé`.

Je vous laisse découvrir seul.e toutes les autres possibilités que vous offre Discord...

## Pour aller plus loin...

* Si vous souhaitez plus d'information sur la configuration des serveurs, [visitez le support de Discord par ici](https://support.discordapp.com/hc/fr/categories/200404378-Configuration-de-serveur).

* La communauté de Discord est très active niveau développement de Bots. Beaucoup de bots pourraient vous être utiles pour votre serveur. [Ici sont listés beaucoup de bots créés par la communauté](https://top.gg).
